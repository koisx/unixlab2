package com.devx.lab4.withCache;

import org.apache.commons.lang.time.StopWatch;

@SuppressWarnings("Duplicates")
public class App {
    private static long A = 0;
    public static void main( String[] args ){
        StopWatch stopWatch = new StopWatch();

        System.out.println("Without critical section:");
        stopWatch.start();
        new Competition().show();
        stopWatch.stop();
        System.out.println(stopWatch.getTime());
        stopWatch.reset();

        System.out.println("\nWith critical section:");
        stopWatch.start();
        new CorrectIncrement().show();
        stopWatch.stop();
        System.out.println(stopWatch.getTime());
        stopWatch.reset();
    }

}
