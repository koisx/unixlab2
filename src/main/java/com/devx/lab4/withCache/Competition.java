package com.devx.lab4.withCache;

@SuppressWarnings("Duplicates")
public class Competition {
    private static long A = 0;
    class MyThread extends Thread {
        @Override
        public void run() {
            for (int i = 1; i <= 10000000; i++) {
                A++;
            }
            System.out.println("finish " + Thread.currentThread().getName());
        }
    }

    public void show() {
        MyThread t1 = new MyThread();
        MyThread t2 = new MyThread();
        MyThread t3 = new MyThread();
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("A=" + A);
    }
}
