package com.devx.lab4.withoutCache;

import java.util.concurrent.locks.ReentrantLock;

@SuppressWarnings("Duplicates")
public class CorrectIncrement {
    private final static ReentrantLock lock = new ReentrantLock();
    private static volatile long A = 0; // Shared Resource
    class MyThread extends Thread {
        @Override
        public void run() {
            for (int i = 1; i <= 10000000; i++) {
                lock.lock();
                try { A++; }
                finally { lock.unlock(); }
            }
            System.out.println("finish " + Thread.currentThread().getName());
        }
    }

    public void show() {
        MyThread t1 = new MyThread();
        MyThread t2 = new MyThread();
        MyThread t3 = new MyThread();
        t1.start();
        t2.start();
        t3.start();
        try { t1.join(); t2.join(); t3.join(); }
        catch (InterruptedException e) { }
        System.out.println("A=" + A); }
}
