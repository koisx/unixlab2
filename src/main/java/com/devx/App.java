package com.devx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.BinaryOperator;

public class App 
{
    public static void main( String[] args ) throws ExecutionException, InterruptedException, IOException {
        System.out.println("Submitting tasks to executors");
        System.out.println("Press any button to terminate the program");

        CalculationManager calculationManager = new CalculationManager();
        Future<Boolean> process1 = calculationManager.calculate(true);
        Future<Boolean> process2 = calculationManager.calculate(false);

        InputStreamReader fileInputStream=new InputStreamReader(System.in);
        BufferedReader bufferedReader=new BufferedReader(fileInputStream);

        while(!(process1.isDone() && process2.isDone())){
            if(process1.isDone() && process1.get()){
                System.out.println("X appeared to be true -> optimization applied");
                System.out.printf("Result:%s\n",true);
                calculationManager.shutdownNow();
                return;
            }
            if(process2.isDone() && process2.get()){
                System.out.println("Y appeared to be true -> optimization applied");
                System.out.printf("Result:%s\n",true);
                calculationManager.shutdownNow();
                return;
            }

            if (bufferedReader.ready()){
                System.out.println("Terminating...");
                calculationManager.shutdownNow();
                return;
            }
        }

        BinaryOperator<Boolean> function = (x,y)-> x || y;
        System.out.println("Calculating resulting value.Wait...");
        System.out.printf("Result:%s\n",function.apply(process1.get(), process2.get()));
        System.out.printf("x=%s, y=%s", process1.get(), process2.get());
        calculationManager.shutdown();
    }
}
