package com.devx;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CalculationManager {
    private ExecutorService executorService = Executors.newFixedThreadPool(2);

    public Future<Boolean> calculate(final boolean value){
        return  executorService.submit(
                ()-> {
                    Thread.sleep(new Random().nextInt(5000));
                    System.out.println("Calculation in one of the executors is finished. Value="+value);
                    return value;
                }
        );
    }

    public void shutdown(){
        if(executorService!=null)
            executorService.shutdown();
    }

    public void shutdownNow(){
        if(executorService!=null)
            executorService.shutdownNow();
    }
}
